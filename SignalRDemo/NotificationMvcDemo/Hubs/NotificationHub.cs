﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace NotificationMvcDemo.Hubs
{
    public class NotificationHub: Hub
    {
        
        //No need code here because only the server send notification to clients
        //public async Task SendMessage(string user, string message)
        //{
        //    await Clients.All.SendAsync("ReceiveMessage", user, message);
        //}
    }
}
