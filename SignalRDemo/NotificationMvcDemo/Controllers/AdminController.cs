﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NotificationMvcDemo.Models;
using NotificationMvcDemo.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace NotificationMvcDemo.Controllers
{
    public class AdminController : Controller
    {
        private readonly IHubContext<NotificationHub> _notificationHubContext;
        public AdminController(IHubContext<NotificationHub> notificationHubContext)
        {
            _notificationHubContext = notificationHubContext;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Index([FromForm] string message)
        {
            await _notificationHubContext.Clients.All.SendAsync("initSignal", message);
            return View();
        }

    }
}
