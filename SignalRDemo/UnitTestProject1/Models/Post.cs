﻿using System;
using System.Collections.Generic;

namespace UnitTestProject1.Models
{
    public partial class Post
    {
        public Post()
        {
            Comment = new HashSet<Comment>();
        }

        public int PostId { get; set; }
        public int BlogId { get; set; }
        public string Tittle { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual Blog Blog { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
    }
}
